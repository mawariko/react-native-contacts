import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      display: 'flex',
      flex: 1,
      backgroundColor:'#F4F3EA'
    },
    searchBox: {
      backgroundColor: '#DFEFCA',
      marginTop: 80,
      marginBottom: 40,
      marginLeft:15,
      marginRight: 15,
      borderRadius: 15,
      height: 50,
      padding: 3,
      borderWidth: 0.5,
      borderColor: '#7d90a0',
      display:'flex',
      flexDirection: 'row'
    },
    searchInput: {
      fontSize: 18,
      marginLeft:30
    },
    itemBox: {
      minHeight: 70,
      padding:5,
      borderBottomColor:'pink',
      borderBottomWidth: 0.5,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: '#CFDB31',
      marginBottom: 10
    },
    itemTextContent: {
      marginLeft:35
    },
    itemText: {
      color: '#02182B',
      fontWeight: 'bold',
      fontSize: 24
    },
    itemEmail: {
      color: '#02182B',
      fontWeight: 'bold'
    },
    infoIcon: {
      marginLeft: 'auto',
      marginRight:5,
    }
});

export default styles;
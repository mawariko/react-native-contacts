import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Button,
  FlatList,
  TextInput,
  ActivityIndicator,
  TouchableOpacity,
  Platform,
  StyleSheet,
  PermissionsAndroid,
} from 'react-native';
import Contacts from 'react-native-contacts';
import styles from './Styles';

export default function Main({navigation, route: { params } }) {
  const [contacts, setContacts] = useState([]);
  const [tempContacts, setTempContacts] = useState([]);
  const [loading, setLoading] = useState(true);
  
  const lastUpdated = params && params.lastUpdated ? params.lastUpdated : null;

  const loadContacts = async () => {
    setLoading(true);
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        console.warn('Permission to access contacts was denied');
      } else {
        setContacts(contacts);
        setTempContacts(contacts);
        setLoading(false);
      }
    });
  };

  useEffect(() => {
    if (Platform.OS === 'android') {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: 'Contacts',
        message: 'This app would like to view your contacts.',
      }).then(() => {
        loadContacts();
      });
    } else {
      loadContacts();
    }
  }, [lastUpdated]);

  const searchHandler = (val) => {
    const filteredContacts = tempContacts.filter((contact) => {
      let contactLowercase = `${contact.givenName} ${contact.familyName}`.toLowerCase();
      let searchTermLowercase = val.toLowerCase();
      return contactLowercase.indexOf(searchTermLowercase) > -1;
    });
    setContacts(filteredContacts);
  };

  const renderItem = ({item}) => {
    const showDetails = () => {
      navigation.navigate('Details', {contact: item});
    };

    return (
      <TouchableOpacity onPress={showDetails}>
        <View style={styles.itemBox}>
          <View style={styles.itemTextContent}>
            <Text style={styles.itemText}>
              {item.givenName + ' '}
              {item.familyName}
            </Text>
            {item.emailAddresses.length > 0 && (
              <Text style={styles.itemEmail}>
                {item.emailAddresses[0].email}
              </Text>
            )}
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchBox}>
        <TextInput
          placeholder="Search"
          placeholderTextColor="grey"
          style={styles.searchInput}
          onChangeText={searchHandler}
        />
      </View>

      <View style={{flex: 1, backgroundColor: 'white'}}>
        {loading ? (
          <View
            style={{
              ...StyleSheet.absoluteFill,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <ActivityIndicator size="large" color="#bad555" />
          </View>
        ) : null}
        <FlatList
          style={{backgroundColor:'#F4F3EA'}}
          data={contacts}
          renderItem={renderItem}
          keyExtractor={(item) => item.recordID}
          ListEmptyComponent={() => (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 50,
              }}>
              <Text style={{color: '#bad555'}}>No Contacts Found</Text>
            </View>
          )}
        />
        <Button onPress={() => navigation.navigate('Details', {contact: null})} title="Add contact" />
      </View>
    </View>
  );
}

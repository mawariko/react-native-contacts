import {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
    detailsContainer: {
        backgroundColor: '#F4F3EA',
        display: 'flex',
        flexDirection: 'column',
        paddingTop: 70,
        flex: 1,
    },
    image: {
        height: 150,
        width: 150, 
        alignSelf: 'center',
        marginBottom: 30,
        borderRadius:10
    },
    label: {
        color: 'grey',
        paddingLeft: 5,
        paddingLeft: 40,
    },
    input: {
        fontSize: 26,
        paddingLeft: 0,
        marginTop: -10,
        marginBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#C1CAD6',
        paddingLeft: 40,
    },
    btnContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 50
    },
    btn: {
        width: 100,
        borderRadius: 5,
    },
    btnSave: {
        width: 200,
        borderRadius: 5,
        alignSelf: 'center',
    }

});

export default styles;
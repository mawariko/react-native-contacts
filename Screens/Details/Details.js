import React, { useState } from 'react';
import {
    View,
    Text,
    Image,
    TextInput,
    Button,
    Linking,
    Platform,
    PermissionsAndroid
} from 'react-native';
import styles from './Styles';
import Contacts from 'react-native-contacts';

export default function Details({navigation, route: { params: { contact } } }) {
    const [name, setName] = useState(contact ? contact.givenName : '');
    const [surname, setSurname] = useState(contact ? contact.familyName: '');
    const [phone, setPhone] = useState(contact && contact.phoneNumbers.length ? contact.phoneNumbers[0].number : '');
    const [email, setEmail] = useState(contact && contact.emailAddresses.length ? contact.emailAddresses[0].email : '');
    const [isChanged, setIsChanged] = useState(false);

    const createContact = () => {
      const newContact = {
        givenName: name,
        familyName: surname,
        phoneNumbers: [
          {
            label: 'mobile',
            number: phone
          }
        ]
      };
      if(email) {
        newContact.emailAddresses = [{ label: 'main', email }];
      }
      Contacts.addContact(newContact, err => {
        if (!err) {
          navigation.navigate('Main', { lastUpdated: (new Date()).getTime() });
        } else {
          console.log(err);
        }
      });
    };

    const updateContact = () => {
      const updatedContact = {
        ...contact,
        givenName: name,
        familyName: surname,
        phoneNumbers: contact.phoneNumbers.map((item, index) => index === 0 ? {...item, number: phone} : item),
        emailAddresses: contact.emailAddresses.length
          ? contact.emailAddresses.map((item, index) => index === 0 ? {...item, email} : item)
          : [{ label: 'main', email }]
      };
      Contacts.updateContact(updatedContact, err => {
        if (!err) {
          setIsChanged(false);
          navigation.navigate('Main', { lastUpdated: (new Date()).getTime() });
        } else {
          console.log(err);
        }
      });
    };

    const deleteContact = () => {
        Contacts.deleteContact(contact, err => {
            if (!err) {
              navigation.navigate('Main', { lastUpdated: (new Date()).getTime() });
            } else {
              console.log(err);
            }
        });
    };

    const requestWritePermissions = async () => {
      if (Platform.OS === 'android') {
        await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS, {
          title: 'Contacts',
          message: 'This app would like to edit your contacts.',
        });
        return true;
      } else {
        return true;
      }
    };

    const handleCall = () => {
      Linking.openURL(`tel:${phone}`);
    };

    const handleSave = async () => {
      const granted = await requestWritePermissions();
      if (granted) {
        contact ? updateContact() : createContact();
      }
    };

    const handleDelete = async () => {
      const granted = await requestWritePermissions();
      if (granted) {
        deleteContact();
      }
    };

    return (
        <View style={styles.detailsContainer}>
            {contact && contact.hasThumbnail && (<Image style={styles.image} source={{uri: contact.thumbnailPath}}></Image>)}
            <Text style={styles.label}>Name</Text>
            <TextInput style={styles.input} onChangeText={val => {setName(val); setIsChanged(true)}}>{name}</TextInput>
            <Text style={styles.label}>Surname</Text>
            <TextInput style={styles.input} onChangeText={val => {setSurname(val); setIsChanged(true)}}>{surname}</TextInput>
            <Text style={styles.label}>Phone number</Text>
            <TextInput style={styles.input} onChangeText={val => {setPhone(val); setIsChanged(true)}}>{phone}</TextInput>
            <Text style={styles.label}>Email</Text>
            <TextInput style={styles.input} onChangeText={val => {setEmail(val); setIsChanged(true)}}>{email}</TextInput>
            {isChanged && <View style={styles.btnSave}><Button title="Save" onPress={handleSave} /></View>}
            {contact && (<View style={styles.btnContainer}>
                <View style={styles.btn}><Button title="Call" onPress={handleCall} /></View>
                <View style={styles.btn}><Button title="Delete" onPress={handleDelete} /></View>
            </View>)}
        </View>
    );
}